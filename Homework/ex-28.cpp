#include <iostream>

using namespace std;

int main() {
    unsigned int year;

    cout << "Enter year number: ";
    cin >> year;

    unsigned int days;
    if (year % 4 == 0) {
        if (year % 100 != 0 || year % 400 == 0) {
            days = 366;
        } else {
            days = 365;
        }
    } else {
        days = 365;
    }
    cout << "Number of days in " << year << " year: " << days << " days" << endl;

    return 0;
}