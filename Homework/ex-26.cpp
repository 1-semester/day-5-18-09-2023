#include <iostream>

using namespace std;

int main()
{
    double x;

    cout << "Enter x: ";
    cin >> x;

    double result;
    if (x <= 0)
    {
        result = -x;
    }
    else if (x > 0 && x < 2)
    {
        result = pow(x, 2);
    }
    else
    {
        result = 4.0;
    }

    cout << "The value of the function f(x) for x = " << x << " equals " << result << endl;

    return 0;
}