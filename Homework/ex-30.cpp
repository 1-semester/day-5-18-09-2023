#include <iostream>

using namespace std;

int main()
{
    unsigned int number;

    cout << "Enter an integer (1-999): ";
    cin >> number;

    if (number >= 1 && number <= 999)
    {
        if (number % 2 == 0)
        {
            if (number >= 1 && number <= 9)
            {
                cout << number << "– even single digit number" << endl;
            }
            else if (number >= 10 && number <= 99)
            {
                cout << number << "– even two-digit number" << endl;
            }
            else if (number >= 100 && number <= 999)
            {
                cout << number << "– even three digit number" << endl;
            }
        }
        else
        {
            if (number >= 1 && number <= 9)
            {
                cout << number << "– odd single digit number" << endl;
            }
            else if (number >= 10 && number <= 99)
            {
                cout << number << "– odd two-digit number" << endl;
            }
            else if (number >= 100 && number <= 999)
            {
                cout << number << "– odd three digit number" << endl;
            }
        }
    }
    else
    {
        cout << "Number out of range" << endl;
    }

    return 0;
}
