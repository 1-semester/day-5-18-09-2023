#include <iostream>

using namespace std;

int main()
{
    double x;
    int result;

    cout << "Enter x: ";
    cin >> x;

    if (x < 0)
    {
        result = 0;
    }
    else if (fmod(x, 2) >= 0 && fmod(x, 2) < 1)
    {
        result = 1;
    }
    else
    {
        result = -1;
    }

    cout << "The value of the function f(x) for x = " << x << " equals " << result << endl;

    return 0;
}